# Playwright Automação Web

![](https://i.ytimg.com/vi/2_BPIA5RgXU/maxresdefault.jpg)

1. Instalar o NodeJs [Clique aqui para baixar](https://nodejs.org/en/download/)

Após a instalação, você pode verificar se foi bem sucedido executando o comando abaixo:
```
npm --version
```

2. Crie um diretório para instalação do playwright
```
mkdir pw
cd pw
npm i -D playwright
```

3. Executar o Playwright Inspector para criar casos de teste Web.
```
npx playwright codegen
```

4. Após a execução do teste e captura no Playwright Inspector, deve ser escolhido a linguagem de execução e adicionado em um arquivo por exemplo um arquivo **meuteste.py**.

5. Para executar, deve ser executado conforme linguagem específica, no exemplo abaixo, executamos o script python:
```
python meuteste.py
```

No código, pode ser adicionado variáveis, funções e outras ações de recursos da linguagem. Um bom exemplo é adicionar variáveis para usuário e senha, ou inserir argumentos na execução do script.
